package ru.nlmk.study.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class MyService {
    private static final String incorrectFormat = "Incorrect number format";

    public long sum(String arg1, String arg2){
        int a, b;
        try {
            a = Integer.parseInt(arg1);
            b = Integer.parseInt(arg2);
            return a + b;
        } catch (NumberFormatException e){
            throw new IllegalArgumentException(incorrectFormat);
        }
    }

    public int getFactorial(String arg){
        try{
            int number = Integer.parseInt(arg);
            return IntStream.rangeClosed(1, number).reduce(1, (x, y) -> x * y);
        } catch(NumberFormatException | ArithmeticException e){
            throw new IllegalArgumentException(incorrectFormat);
        }
    }

    public Long[] fibonacci(String arg){
        try {
            int n = Integer.parseInt(arg);
            if (n <= 0) {
                throw new IllegalArgumentException("Number is less then 0.");
            }
            if(Math.sqrt(5*n*n-4) % 1 == 0 || Math.sqrt(5*n*n+4) % 1 == 0){//если условие выполняется, то число явялется числом Фибоначчи.
                List<Long> result = new ArrayList<>();
                result.add(1L);
                result.add(1L);
                for(int i = 2, j = 2; j <= n; i++, j = (int)(result.get(i-2)+result.get(i-1))){
                    result.add(result.get(i-2)+result.get(i-1));
                }
                Long[] l = new Long[result.size()];
                for(int i = 0; i < result.size(); i++){
                    l[i] = result.get(i);
                }
                return l;
            } else{
                throw new IllegalArgumentException();
            }
        } catch(NumberFormatException e){
            throw new IllegalArgumentException(incorrectFormat);
        }
    }

}
