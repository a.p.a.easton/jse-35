package ru.nlmk.study;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.nlmk.study.service.MyService;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class MyServiceTest {

    MyService myService;

    @BeforeEach
    void setup(){
        myService = new MyService();
    }

    @Test
    void sum() {
        long l = myService.sum("1","3");
        assertEquals(1+3,l);
    }

    @Test
    void sumExeption() {
        assertThrows(IllegalArgumentException.class, () -> myService.sum("b", "1"));
    }

    @Test
    void factorial() {
        assertEquals(6, myService.getFactorial("3"));
    }

    @Test
    void factorialExeptions() {
        assertThrows(IllegalArgumentException.class, () -> myService.getFactorial(","));
    }

    /*@Test
    void factorialOverflow() {
        assertThrows(IllegalArgumentException.class, () -> myService.factorial("50", "1"));
    }*/

    @Test
    void fibonacci() {
        Long[] expected = new Long[]{1L,1L,2L,3L,5L};
        Long[] actual = myService.fibonacci("5");
        assertArrayEquals(expected, actual);
    }

    @Test
    void fibonacciExeptions() {
        assertThrows(IllegalArgumentException.class, () -> myService.fibonacci(","));
    }
}